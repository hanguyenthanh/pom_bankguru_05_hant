package page.ui;

public class HomePageUI {

	public static final String WELCOME_MESSAGE = "//marquee[text()=\"Welcome To Manager's Page of Guru99 Bank\"]";
	public static final String MANAGER_MENU_ITEM = "//a[text()='Manager']";
	public static final String NEW_CUSTOMER_MENU_ITEM = "//a[text()='New Customer']";
	public static final String EDIT_CUSTOMER_MENU_ITEM = "//a[text()='Edit Customer']";
	public static final String DELETE_CUSTOMER_MENU_ITEM = "//a[text()='Delete Customer']";
	public static final String NEW_ACCOUNT_MENU_ITEM = "//a[text()='New Account']";
	public static final String EDIT_ACCOUNT_MENU_ITEM = "//a[text()='Edit Account']";
	public static final String DELETE_ACCOUNT_MENU_ITEM = "//a[text()='Delete Account']";
	public static final String DEPOSIT_MENU_ITEM = "//a[text()='Deposit']";
	public static final String WITHDRAWAL_MENU_ITEM = "//a[text()='Withdrawal']";
	public static final String FUND_TRANSFER_MENU_ITEM = "//a[text()='Fund Transfer']";
	public static final String CHANGE_PASSWORD_MENU_ITEM = "//a[text()='Change Password']";
	public static final String BALANCE_ENQUIRY_MENU_ITEM = "//a[text()='Balance Enquiry']";
	public static final String MINI_STATEMENT_MENU_ITEM = "//a[text()='Mini Statement']";
	public static final String CUSTOMISED_STATEMENT_MENU_ITEM = "//a[text()='Customised Statement']";
	public static final String LOGOUT_MENU_ITEM = "//a[text()='Log out']";
	
}
