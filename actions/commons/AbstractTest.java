package commons;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AbstractTest {

	WebDriver driver;
	
	public WebDriver openMultiBrowsers(String browser, String url) {
		
		if (browser.equals("firefox")) {

			driver = new FirefoxDriver();

		} else if (browser.equals("chrome")) {

			System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
			driver = new ChromeDriver();

		} else if (browser.equals("headless")) {

			System.setProperty("webdriver.ie.driver", ".\\resources\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
			options.addArguments("window-size=1366x766");
			driver = new ChromeDriver(options);
		}
		driver.manage().window().maximize();
		driver.get(url);		
		return driver;
	}
	
}
