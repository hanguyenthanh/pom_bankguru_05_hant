package pageObjects;

import org.openqa.selenium.WebDriver;
import commons.AbstractPage;
import page.ui.LoginPageUI;

public class LoginPageObject extends AbstractPage {

	WebDriver driver;
	
	public LoginPageObject (WebDriver mappingDriver) {
		
		driver = mappingDriver;
	}
	
	
	public String getLoginPageURL() {
		
		return getCurrentURL(driver);
	} 
	
	public void inputEmailAddressTextbox(String email) {
		
		waitForControlVisible(driver, LoginPageUI.USERID_TEXTBOX);
		sendkeyToElement(driver, LoginPageUI.USERID_TEXTBOX, email);
		
	}
	
	public void inputPasswordTextbox(String password) {
		
		waitForControlVisible(driver, LoginPageUI.PASSWORD_TEXTBOX);
		sendkeyToElement(driver, LoginPageUI.PASSWORD_TEXTBOX, password);
		
	}
	
	public void clickLoginButton() {

		waitForControlVisible(driver, LoginPageUI.LOGIN_BUTTON);
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);

	}

	public void clickHereLinkToOpenRegisterPage() {
		
		waitForControlVisible(driver, LoginPageUI.HERE_LINK);
		clickToElement(driver, LoginPageUI.HERE_LINK);
				
		
	}
	
}
