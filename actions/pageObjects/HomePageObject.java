package pageObjects;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import page.ui.HomePageUI;
import page.ui.LoginPageUI;

public class HomePageObject extends AbstractPage {

	WebDriver driver;

	public HomePageObject(WebDriver mappingDriver) {

		driver = mappingDriver;
	}
	
	public boolean isHomePageDisplayed() {
		
		waitForControlVisible(driver, HomePageUI.WELCOME_MESSAGE);
		return isControlDisplayed(driver, HomePageUI.WELCOME_MESSAGE);
		
	}
	
	public void openManagerPage() {
		
		waitForControlVisible(driver, HomePageUI.MANAGER_MENU_ITEM);
		clickToElement(driver, HomePageUI.MANAGER_MENU_ITEM);
		
	}
	
	public void openNewCustomerPage() {
		
		waitForControlVisible(driver, HomePageUI.NEW_CUSTOMER_MENU_ITEM);
		clickToElement(driver, HomePageUI.NEW_CUSTOMER_MENU_ITEM);
		
	}
	
	public void openEditCustomerPage() {
		
		waitForControlVisible(driver, HomePageUI.EDIT_CUSTOMER_MENU_ITEM);
		clickToElement(driver, HomePageUI.EDIT_CUSTOMER_MENU_ITEM);
		
	}
	
	public void openDeleteCustomerPage() {
		
		waitForControlVisible(driver, HomePageUI.DELETE_CUSTOMER_MENU_ITEM);
		clickToElement(driver, HomePageUI.DELETE_CUSTOMER_MENU_ITEM);
		
	}


}
